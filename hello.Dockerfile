FROM openjdk:11
EXPOSE 8080
CMD java $JAVA_OPTS -jar /service.jar
COPY "build/libs/demo-0.0.1-SNAPSHOT.jar" /service.jar